extends Label

var time = 0.0

func _ready():
	set_process(true)
	if(Globals.get("curLvl") != 1):
		Globals.set("addAmount", Globals.get("lvlUpAmount") / 2)
		Globals.set("lvlUpAmount", Globals.get("lvlUpAmount") + Globals.get("addAmount"))

func _process(delta):
	if(Globals.get("curLvl") == 4):
		self.get_tree().change_scene("res://scenes/End.tscn")
		
	self.set_text("Level " + String(Globals.get("curLvl")) + "\nScore Needed: " + String(Globals.get("lvlUpAmount")))
	
	time += delta
	if(time >= 2):
		self.get_tree().change_scene("res://scenes/lvl" + String(Globals.get("curLvl")) + ".tscn")