extends Area2D

func _ready():
	#set_process(true)
	set_fixed_process(true)
	
func _fixed_process(delta):
	get_parent().set_offset(get_parent().get_offset() + (50 * delta))

func _on_Guard_body_enter(body):
	if(body.get_name() == "Player"):
		var directState = self.get_world_2d().get_direct_space_state()
		var collisions = directState.intersect_ray(self.get_pos(), body.get_pos(), [self])
		if(collisions.size() && collisions["collider"].get_name() == "Player"):
			self.get_tree().change_scene("res://scenes/GameOver.tscn")