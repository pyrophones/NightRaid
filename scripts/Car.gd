extends Area2D

func _ready():
	pass

func _on_CarArea_body_enter( body ):
	if(body.get_name() == "Player"):
		body.nearCar = true

func _on_CarArea_body_exit( body ):
	if(body.get_name() == "Player"):
		body.nearCar = false