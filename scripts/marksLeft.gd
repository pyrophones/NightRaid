extends Label

func _ready():
	set_process(true)
	
func _process(delta):
	self.set_text("Marks: " + String(self.get_parent().get_parent().get_node("Player").marks))