extends Button

func _ready():
	pass

func _on_play_pressed():
	Globals.set("curLvl", 1)
	Globals.set("lvlUpAmount", 5000)
	self.get_tree().change_scene("res://scenes/lvlChange.tscn")

func _on_PlayAgain_pressed():
	if(Globals.get("curLvl") != 1):
		Globals.set("lvlUpAmount", Globals.get("lvlUpAmount") - Globals.get("addAmount"))
	self.get_tree().change_scene("res://scenes/lvlChange.tscn")
	