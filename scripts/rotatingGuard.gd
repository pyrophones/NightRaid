extends Area2D

export var rotSpeed = 2 * PI / 4
export var maxTime = 7
var curPoint
var timer = 0.0

func _ready():
	set_fixed_process(true)
	curPoint = self.get_child(3)

func _fixed_process(delta):
	
	var dir = Vector2()
	dir.x = sin(self.get_rot())
	dir.y = cos(self.get_rot())
	var other = curPoint.get_pos() - dir
	var x = dir.normalized().dot(other.normalized())
	if(x >= 0.99 && x <= 1.0):
		if(timer >= maxTime):
			if(curPoint == self.get_child(3)):
				curPoint = self.get_child(4)
				rotSpeed = -rotSpeed
				timer = 0
				return
			elif(curPoint == self.get_child(4)):
				curPoint = self.get_child(3)
				rotSpeed = -rotSpeed
				timer = 0
				return
			
	else:
		rotate(rotSpeed * delta)
	
	timer += delta

func _on_RotateGuard_body_enter(body):
	if(body.get_name() == "Player"):
		var directState = self.get_world_2d().get_direct_space_state()
		var collisions = directState.intersect_ray(self.get_pos(), body.get_pos(), [self])
		if(collisions.size() && collisions["collider"].get_name() == "Player"):
				self.get_tree().change_scene("res://scenes/GameOver.tscn")
