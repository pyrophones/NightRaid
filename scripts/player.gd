extends KinematicBody2D

var tilePos = Vector2()
var marks = 3
var items = 0
var score = 0
var nearCar = false

func _ready():
	set_process(true)
	set_process_input(true)
	pass

func _process(delta):
	var mv = Vector2(0, 0)
	var b
	if(is_colliding()):
		b = get_collider()
		if(b.is_in_group("TileMap")):
			b = get_collider_metadata()
			tilePos = b

	if(Input.is_action_pressed("move_forward")):
		mv += Vector2(0, -1)
	if(Input.is_action_pressed("move_back")):
		mv += Vector2(0, 1)
	if(Input.is_action_pressed("move_left")):
		mv += Vector2(-1, 0)
	if(Input.is_action_pressed("move_right")):
		mv += Vector2(1, 0)
		
	if(Input.is_action_pressed("run")):
		mv *= 3
		
	move(mv)

func _input(event):
	if(marks > 0 && event.type == InputEvent.KEY && event.scancode == KEY_SPACE):
		var tMap = self.get_parent().get_child(0)
		var cell = tMap.get_cellv(tilePos)
		if(cell == 2 || cell == 3 || cell == 10):
			tMap.set_cellv(tilePos, 9)
			marks -= 1
			items += 1
	if(nearCar && event.type == InputEvent.KEY && event.scancode == KEY_SPACE):
		for i in range(items):
			score += 1000
			marks += 1
			items -= 1