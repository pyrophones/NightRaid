extends Node


func _ready():
	set_process(true)

func _process(delta):
	if(self.get_child(1).get_name() == "Player"):
		if(self.get_child(1).score >= Globals.get("lvlUpAmount")):
			Globals.set("curLvl", Globals.get("curLvl") + 1)
			self.get_tree().change_scene("res://scenes/lvlChange.tscn")